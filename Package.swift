// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "MOAnalyticsSample",
    platforms: [.iOS(.v10)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "MOAnalyticsSample",
            targets:["MOAnalyticsSample"]),
    ],
    dependencies: [
         .package(name:"MoEngageCore", url: "https://rakshithaachar1996@bitbucket.org/rakshithaachar1996/moengagecore.git", from: "1.0.0"),
        
         .package(name: "Files",
                    url: "https://github.com/johnsundell/files.git",
                    from: "4.0.0"
                )
    ],
    targets: [
        .target(name: "MOAnalyticsSample", dependencies: ["MoEngageCore", "Files"])

//        .binaryTarget(name: "MOAnalytics", url: "https://inappweb-cdd70.web.app/MOAnalytics.xcframework.zip", checksum: "75e83e673551c0b2a435013bd344f5e237df61963fc684469f8958076225bbdc"),

    ]
)

